# GitBook Plugin

为感兴趣的链接, 在页面内添加快捷跳转链接, 无需刷新页面;


## 页面使用
1. 在当前页面可以添加和取消当前页面的关注
2. 在左上方展示已关注的列表, 点击可切换页面
3. 预览页面见`perview.png`


## 使用方法
使用此插件需要在运行环境支持localStorage, 用于缓存链接信息以便在下次打开时展现;

使用此插件需要修改**gitbook-plugin-theme-default**中的**navigation**模块的`module.exports`
修改前
```
module.exports = {
    init: init,
    goNext: goNext,
    goPrev: goPrev
};
```
修改后
```
module.exports = {
    init: init,
    goNext: goNext,
    goPrev: goPrev,
    toPage:handleNavigation
};

```

在`book.json`中添加
```
{
    "plugins": [
        "add-to-interest"
    ]
}
```


## 实现说明
1. 在"修改字体"(.font-settings)的按钮后添加一个"添加关注"按钮
2. 在左侧导航列表之前(nav[role=navigation])添加一个列表用于展示关注的链接

## 免责声明
1. 如使用者将本项目或文章内容中的部分或全部用于违法行为的, 一切后果使用者承担，此项目或文章作者不承担任何责任。
2. 如使用者使用本项目或文章内容中的部分或全部构建的开发成果被用于违法目的, 一切后果使用者承担，此项目或文章作者不承担任何责任。
