require(['gitbook', 'jquery'], function(gitbook, $) {
	var interested =false;
    var INTEREST_STORE_KEY = 'gitbook.plugins.interest';
    var interests = [];
    var buttonTextArr = ['添加关注','取消关注'];
    var textOfNoInterest = '暂无关注';

    function viewInterests(){
		var nav = document.querySelector('.book-summary nav[role=navigation]');
		var interestUl = nav.querySelector('.add-to-interest-ul');
		if(!interestUl){
			interestUl = document.createElement('ul');
			interestUl.classList.add('add-to-interest-ul');
			nav.prepend(interestUl);
		}else{
			interestUl.innerHTML = '';
		}

		if(!interests||!interests.length){
			var li = document.createElement('li');
			li.innerText = textOfNoInterest;
			interestUl.appendChild(li);
			return;
		}
		interests.forEach(interest=>{
			var li = document.createElement('li');
			var a = document.createElement('a');
			a.href=interest.url;
			a.innerText = interest.title;
			a.classList.add('add-to-interest-link')
			if(interest.url == window.location.pathname)
				a.classList.add('active')
			li.appendChild(a);
			interestUl.appendChild(li);
		})
		$(document).on('click', '.add-to-interest-link', e=>{e.preventDefault();gitbook.navigation.toPage(e.target.getAttribute('href'),true);});
    }

    function initButton(){
		var fontSettingsButton = document.querySelector('.book-body .book-header .font-settings');
		var addButton = document.createElement('a');

		addButton.classList.add('btn','pull-left','js-toolbar-action','add-to-interest-add-button');
		addButton.innerText = buttonTextArr[interested?1:0];
		fontSettingsButton.after(addButton)

		$(addButton).on('click',function(event){
			if(interested){
				interests = interests.filter(fav=>{return fav.url !== window.location.pathname});
			}else{
				interests.push({
					title:gitbook.page.getState().chapterTitle,
					url:window.location.pathname
				})
			}
			interested = !interested;
			event.target.innerText = buttonTextArr[interested?1:0];

			localStorage.setItem(INTEREST_STORE_KEY, JSON.stringify(interests));
			viewInterests();
		})
    }

    function checkEnvironment(){
    	return window && window['localStorage']
    	 && window['localStorage'].getItem && window['localStorage'].getItem.apply
    	 && window['localStorage'].setItem && window['localStorage'].setItem.apply
    	 && window['JSON'].parse && window['JSON'].parse.apply
    	 && window['JSON'].stringify && window['JSON'].stringify.apply
    	 ;
    }

    gitbook.events.bind('start', function(e, config) {
    	if(!checkEnvironment()) return;

		interests = JSON.parse(localStorage.getItem(INTEREST_STORE_KEY) || "[]");
    });
    gitbook.events.bind('page.change', function() {
    	if(!checkEnvironment()) return;

		interested = interests.find(interest=>{return interest.url == window.location.pathname});
		
		viewInterests();
		initButton();
    });

});
